﻿namespace EstadisticasEscuelaFrontEnd.Alumnos
{
    partial class frmAlumnoModificar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlumnoModificar));
            this.Datos = new System.Windows.Forms.GroupBox();
            this.txtAlumnoModificarNombre = new System.Windows.Forms.TextBox();
            this.lblAlumnoNuevoNombre = new System.Windows.Forms.Label();
            this.txtAlumnoModificarApellido = new System.Windows.Forms.TextBox();
            this.txtAlumnoModificarLegajo = new System.Windows.Forms.TextBox();
            this.lblAlumnoModificarDniError = new System.Windows.Forms.Label();
            this.lblAlumnoModificarLegajoError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoApellido = new System.Windows.Forms.Label();
            this.lblAlumnoModificarApellidoError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoLegajo = new System.Windows.Forms.Label();
            this.lblAlumnoModificarNombreError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoDNI = new System.Windows.Forms.Label();
            this.txtAlumnoModificarDNI = new System.Windows.Forms.TextBox();
            this.lblAlumnoModificarError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoCursoError = new System.Windows.Forms.Label();
            this.btnAlumnoModificarCancelar = new System.Windows.Forms.Button();
            this.btnAlumnoModificarLimpiar = new System.Windows.Forms.Button();
            this.btnAlumnoModificarAceptar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Datos.SuspendLayout();
            this.SuspendLayout();
            // 
            // Datos
            // 
            this.Datos.Controls.Add(this.label2);
            this.Datos.Controls.Add(this.textBox2);
            this.Datos.Controls.Add(this.textBox1);
            this.Datos.Controls.Add(this.label1);
            this.Datos.Controls.Add(this.txtAlumnoModificarNombre);
            this.Datos.Controls.Add(this.lblAlumnoNuevoNombre);
            this.Datos.Controls.Add(this.txtAlumnoModificarApellido);
            this.Datos.Controls.Add(this.txtAlumnoModificarLegajo);
            this.Datos.Controls.Add(this.lblAlumnoModificarDniError);
            this.Datos.Controls.Add(this.lblAlumnoModificarLegajoError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoApellido);
            this.Datos.Controls.Add(this.lblAlumnoModificarApellidoError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoLegajo);
            this.Datos.Controls.Add(this.lblAlumnoModificarNombreError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoDNI);
            this.Datos.Controls.Add(this.txtAlumnoModificarDNI);
            this.Datos.Location = new System.Drawing.Point(12, 12);
            this.Datos.Name = "Datos";
            this.Datos.Size = new System.Drawing.Size(467, 248);
            this.Datos.TabIndex = 33;
            this.Datos.TabStop = false;
            this.Datos.Text = "Datos";
            // 
            // txtAlumnoModificarNombre
            // 
            this.txtAlumnoModificarNombre.Location = new System.Drawing.Point(128, 26);
            this.txtAlumnoModificarNombre.Name = "txtAlumnoModificarNombre";
            this.txtAlumnoModificarNombre.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoModificarNombre.TabIndex = 4;
            // 
            // lblAlumnoNuevoNombre
            // 
            this.lblAlumnoNuevoNombre.AutoSize = true;
            this.lblAlumnoNuevoNombre.Location = new System.Drawing.Point(27, 33);
            this.lblAlumnoNuevoNombre.Name = "lblAlumnoNuevoNombre";
            this.lblAlumnoNuevoNombre.Size = new System.Drawing.Size(54, 13);
            this.lblAlumnoNuevoNombre.TabIndex = 0;
            this.lblAlumnoNuevoNombre.Text = "NOMBRE";
            // 
            // txtAlumnoModificarApellido
            // 
            this.txtAlumnoModificarApellido.Location = new System.Drawing.Point(128, 63);
            this.txtAlumnoModificarApellido.Name = "txtAlumnoModificarApellido";
            this.txtAlumnoModificarApellido.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoModificarApellido.TabIndex = 5;
            // 
            // txtAlumnoModificarLegajo
            // 
            this.txtAlumnoModificarLegajo.Location = new System.Drawing.Point(128, 96);
            this.txtAlumnoModificarLegajo.Name = "txtAlumnoModificarLegajo";
            this.txtAlumnoModificarLegajo.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoModificarLegajo.TabIndex = 6;
            // 
            // lblAlumnoModificarDniError
            // 
            this.lblAlumnoModificarDniError.AutoSize = true;
            this.lblAlumnoModificarDniError.Location = new System.Drawing.Point(363, 133);
            this.lblAlumnoModificarDniError.Name = "lblAlumnoModificarDniError";
            this.lblAlumnoModificarDniError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoModificarDniError.TabIndex = 20;
            // 
            // lblAlumnoModificarLegajoError
            // 
            this.lblAlumnoModificarLegajoError.AutoSize = true;
            this.lblAlumnoModificarLegajoError.Location = new System.Drawing.Point(363, 99);
            this.lblAlumnoModificarLegajoError.Name = "lblAlumnoModificarLegajoError";
            this.lblAlumnoModificarLegajoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoModificarLegajoError.TabIndex = 19;
            // 
            // lblAlumnoNuevoApellido
            // 
            this.lblAlumnoNuevoApellido.AutoSize = true;
            this.lblAlumnoNuevoApellido.Location = new System.Drawing.Point(27, 70);
            this.lblAlumnoNuevoApellido.Name = "lblAlumnoNuevoApellido";
            this.lblAlumnoNuevoApellido.Size = new System.Drawing.Size(59, 13);
            this.lblAlumnoNuevoApellido.TabIndex = 7;
            this.lblAlumnoNuevoApellido.Text = "APELLIDO";
            // 
            // lblAlumnoModificarApellidoError
            // 
            this.lblAlumnoModificarApellidoError.AutoSize = true;
            this.lblAlumnoModificarApellidoError.Location = new System.Drawing.Point(363, 66);
            this.lblAlumnoModificarApellidoError.Name = "lblAlumnoModificarApellidoError";
            this.lblAlumnoModificarApellidoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoModificarApellidoError.TabIndex = 18;
            // 
            // lblAlumnoNuevoLegajo
            // 
            this.lblAlumnoNuevoLegajo.AutoSize = true;
            this.lblAlumnoNuevoLegajo.Location = new System.Drawing.Point(27, 103);
            this.lblAlumnoNuevoLegajo.Name = "lblAlumnoNuevoLegajo";
            this.lblAlumnoNuevoLegajo.Size = new System.Drawing.Size(48, 13);
            this.lblAlumnoNuevoLegajo.TabIndex = 8;
            this.lblAlumnoNuevoLegajo.Text = "LEGAJO";
            // 
            // lblAlumnoModificarNombreError
            // 
            this.lblAlumnoModificarNombreError.AutoSize = true;
            this.lblAlumnoModificarNombreError.Location = new System.Drawing.Point(363, 29);
            this.lblAlumnoModificarNombreError.Name = "lblAlumnoModificarNombreError";
            this.lblAlumnoModificarNombreError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoModificarNombreError.TabIndex = 17;
            // 
            // lblAlumnoNuevoDNI
            // 
            this.lblAlumnoNuevoDNI.AutoSize = true;
            this.lblAlumnoNuevoDNI.Location = new System.Drawing.Point(27, 141);
            this.lblAlumnoNuevoDNI.Name = "lblAlumnoNuevoDNI";
            this.lblAlumnoNuevoDNI.Size = new System.Drawing.Size(26, 13);
            this.lblAlumnoNuevoDNI.TabIndex = 9;
            this.lblAlumnoNuevoDNI.Text = "DNI";
            // 
            // txtAlumnoModificarDNI
            // 
            this.txtAlumnoModificarDNI.Location = new System.Drawing.Point(128, 134);
            this.txtAlumnoModificarDNI.Name = "txtAlumnoModificarDNI";
            this.txtAlumnoModificarDNI.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoModificarDNI.TabIndex = 10;
            // 
            // lblAlumnoModificarError
            // 
            this.lblAlumnoModificarError.AutoSize = true;
            this.lblAlumnoModificarError.Location = new System.Drawing.Point(108, 223);
            this.lblAlumnoModificarError.Name = "lblAlumnoModificarError";
            this.lblAlumnoModificarError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoModificarError.TabIndex = 32;
            // 
            // lblAlumnoNuevoCursoError
            // 
            this.lblAlumnoNuevoCursoError.AutoSize = true;
            this.lblAlumnoNuevoCursoError.Location = new System.Drawing.Point(479, 228);
            this.lblAlumnoNuevoCursoError.Name = "lblAlumnoNuevoCursoError";
            this.lblAlumnoNuevoCursoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoCursoError.TabIndex = 31;
            // 
            // btnAlumnoModificarCancelar
            // 
            this.btnAlumnoModificarCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAlumnoModificarCancelar.Location = new System.Drawing.Point(403, 289);
            this.btnAlumnoModificarCancelar.Name = "btnAlumnoModificarCancelar";
            this.btnAlumnoModificarCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoModificarCancelar.TabIndex = 30;
            this.btnAlumnoModificarCancelar.Text = "Cancelar";
            this.btnAlumnoModificarCancelar.UseVisualStyleBackColor = true;
            this.btnAlumnoModificarCancelar.Click += new System.EventHandler(this.btnAlumnoModificarCancelar_Click);
            // 
            // btnAlumnoModificarLimpiar
            // 
            this.btnAlumnoModificarLimpiar.Location = new System.Drawing.Point(12, 289);
            this.btnAlumnoModificarLimpiar.Name = "btnAlumnoModificarLimpiar";
            this.btnAlumnoModificarLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoModificarLimpiar.TabIndex = 29;
            this.btnAlumnoModificarLimpiar.Text = "Limpiar";
            this.btnAlumnoModificarLimpiar.UseVisualStyleBackColor = true;
            this.btnAlumnoModificarLimpiar.Click += new System.EventHandler(this.btnAlumnoModificarLimpiar_Click);
            // 
            // btnAlumnoModificarAceptar
            // 
            this.btnAlumnoModificarAceptar.Location = new System.Drawing.Point(312, 289);
            this.btnAlumnoModificarAceptar.Name = "btnAlumnoModificarAceptar";
            this.btnAlumnoModificarAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoModificarAceptar.TabIndex = 28;
            this.btnAlumnoModificarAceptar.Text = "Aceptar";
            this.btnAlumnoModificarAceptar.UseVisualStyleBackColor = true;
            this.btnAlumnoModificarAceptar.Click += new System.EventHandler(this.btnAlumnoModificarAceptar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "label2";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 168);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 22;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(128, 208);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 35;
            // 
            // frmAlumnoModificar
            // 
            this.AcceptButton = this.btnAlumnoModificarAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnAlumnoModificarCancelar;
            this.ClientSize = new System.Drawing.Size(490, 324);
            this.Controls.Add(this.Datos);
            this.Controls.Add(this.lblAlumnoModificarError);
            this.Controls.Add(this.lblAlumnoNuevoCursoError);
            this.Controls.Add(this.btnAlumnoModificarCancelar);
            this.Controls.Add(this.btnAlumnoModificarLimpiar);
            this.Controls.Add(this.btnAlumnoModificarAceptar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAlumnoModificar";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AlumnoModificar";
            this.Load += new System.EventHandler(this.frmAlumnoModificar_Load);
            this.Datos.ResumeLayout(false);
            this.Datos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox Datos;
        private System.Windows.Forms.TextBox txtAlumnoModificarNombre;
        private System.Windows.Forms.Label lblAlumnoNuevoNombre;
        private System.Windows.Forms.TextBox txtAlumnoModificarApellido;
        private System.Windows.Forms.TextBox txtAlumnoModificarLegajo;
        private System.Windows.Forms.Label lblAlumnoModificarDniError;
        private System.Windows.Forms.Label lblAlumnoModificarLegajoError;
        private System.Windows.Forms.Label lblAlumnoNuevoApellido;
        private System.Windows.Forms.Label lblAlumnoModificarApellidoError;
        private System.Windows.Forms.Label lblAlumnoNuevoLegajo;
        private System.Windows.Forms.Label lblAlumnoModificarNombreError;
        private System.Windows.Forms.Label lblAlumnoNuevoDNI;
        private System.Windows.Forms.TextBox txtAlumnoModificarDNI;
        private System.Windows.Forms.Label lblAlumnoModificarError;
        private System.Windows.Forms.Label lblAlumnoNuevoCursoError;
        private System.Windows.Forms.Button btnAlumnoModificarCancelar;
        private System.Windows.Forms.Button btnAlumnoModificarLimpiar;
        private System.Windows.Forms.Button btnAlumnoModificarAceptar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
    }
}