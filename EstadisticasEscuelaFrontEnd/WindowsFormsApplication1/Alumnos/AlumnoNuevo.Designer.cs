﻿namespace EstadisticasEscuelaFrontEnd.Alumnos
{
    partial class frmAlumnoNuevo
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlumnoNuevo));
            this.lblAlumnoNuevoNombre = new System.Windows.Forms.Label();
            this.btnAlumnoNuevo = new System.Windows.Forms.Button();
            this.btnAlumnoNuevoLimpiar = new System.Windows.Forms.Button();
            this.btnAlumnoNuevoCancelar = new System.Windows.Forms.Button();
            this.txtAlumnoNuevoNombre = new System.Windows.Forms.TextBox();
            this.txtAlumnoNuevoApellido = new System.Windows.Forms.TextBox();
            this.txtAlumnoNuevoLegajo = new System.Windows.Forms.TextBox();
            this.lblAlumnoNuevoApellido = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoLegajo = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoDNI = new System.Windows.Forms.Label();
            this.txtAlumnoNuevoDNI = new System.Windows.Forms.TextBox();
            this.lblAlumnoNuevoNombreError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoApellidoError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoLegajoError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoDniError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoCursoError = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoError = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.Datos = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnAlumnoNuevoBuscarUsuario = new System.Windows.Forms.Button();
            this.btnAlumnoNuevoBuscarCurso = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblAlumnoNuevoUsuario = new System.Windows.Forms.Label();
            this.lblAlumnoNuevoCurso = new System.Windows.Forms.Label();
            this.Datos.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAlumnoNuevoNombre
            // 
            this.lblAlumnoNuevoNombre.AutoSize = true;
            this.lblAlumnoNuevoNombre.Location = new System.Drawing.Point(27, 33);
            this.lblAlumnoNuevoNombre.Name = "lblAlumnoNuevoNombre";
            this.lblAlumnoNuevoNombre.Size = new System.Drawing.Size(54, 13);
            this.lblAlumnoNuevoNombre.TabIndex = 0;
            this.lblAlumnoNuevoNombre.Text = "NOMBRE";
            // 
            // btnAlumnoNuevo
            // 
            this.btnAlumnoNuevo.Location = new System.Drawing.Point(313, 282);
            this.btnAlumnoNuevo.Name = "btnAlumnoNuevo";
            this.btnAlumnoNuevo.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoNuevo.TabIndex = 1;
            this.btnAlumnoNuevo.Text = "Aceptar";
            this.btnAlumnoNuevo.UseVisualStyleBackColor = true;
            this.btnAlumnoNuevo.Click += new System.EventHandler(this.btnAlumnoNuevo_Click);
            // 
            // btnAlumnoNuevoLimpiar
            // 
            this.btnAlumnoNuevoLimpiar.Location = new System.Drawing.Point(12, 282);
            this.btnAlumnoNuevoLimpiar.Name = "btnAlumnoNuevoLimpiar";
            this.btnAlumnoNuevoLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoNuevoLimpiar.TabIndex = 2;
            this.btnAlumnoNuevoLimpiar.Text = "Limpiar";
            this.btnAlumnoNuevoLimpiar.UseVisualStyleBackColor = true;
            this.btnAlumnoNuevoLimpiar.Click += new System.EventHandler(this.btnAlumnoNuevoLimpiar_Click);
            // 
            // btnAlumnoNuevoCancelar
            // 
            this.btnAlumnoNuevoCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAlumnoNuevoCancelar.Location = new System.Drawing.Point(404, 282);
            this.btnAlumnoNuevoCancelar.Name = "btnAlumnoNuevoCancelar";
            this.btnAlumnoNuevoCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoNuevoCancelar.TabIndex = 3;
            this.btnAlumnoNuevoCancelar.Text = "Cancelar";
            this.btnAlumnoNuevoCancelar.UseVisualStyleBackColor = true;
            this.btnAlumnoNuevoCancelar.Click += new System.EventHandler(this.btnAlumnoNuevoCancelar_Click);
            // 
            // txtAlumnoNuevoNombre
            // 
            this.txtAlumnoNuevoNombre.Location = new System.Drawing.Point(128, 26);
            this.txtAlumnoNuevoNombre.Name = "txtAlumnoNuevoNombre";
            this.txtAlumnoNuevoNombre.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoNuevoNombre.TabIndex = 4;
            // 
            // txtAlumnoNuevoApellido
            // 
            this.txtAlumnoNuevoApellido.Location = new System.Drawing.Point(128, 63);
            this.txtAlumnoNuevoApellido.Name = "txtAlumnoNuevoApellido";
            this.txtAlumnoNuevoApellido.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoNuevoApellido.TabIndex = 5;
            // 
            // txtAlumnoNuevoLegajo
            // 
            this.txtAlumnoNuevoLegajo.Location = new System.Drawing.Point(128, 96);
            this.txtAlumnoNuevoLegajo.Name = "txtAlumnoNuevoLegajo";
            this.txtAlumnoNuevoLegajo.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoNuevoLegajo.TabIndex = 6;
            // 
            // lblAlumnoNuevoApellido
            // 
            this.lblAlumnoNuevoApellido.AutoSize = true;
            this.lblAlumnoNuevoApellido.Location = new System.Drawing.Point(27, 70);
            this.lblAlumnoNuevoApellido.Name = "lblAlumnoNuevoApellido";
            this.lblAlumnoNuevoApellido.Size = new System.Drawing.Size(59, 13);
            this.lblAlumnoNuevoApellido.TabIndex = 7;
            this.lblAlumnoNuevoApellido.Text = "APELLIDO";
            // 
            // lblAlumnoNuevoLegajo
            // 
            this.lblAlumnoNuevoLegajo.AutoSize = true;
            this.lblAlumnoNuevoLegajo.Location = new System.Drawing.Point(27, 103);
            this.lblAlumnoNuevoLegajo.Name = "lblAlumnoNuevoLegajo";
            this.lblAlumnoNuevoLegajo.Size = new System.Drawing.Size(48, 13);
            this.lblAlumnoNuevoLegajo.TabIndex = 8;
            this.lblAlumnoNuevoLegajo.Text = "LEGAJO";
            // 
            // lblAlumnoNuevoDNI
            // 
            this.lblAlumnoNuevoDNI.AutoSize = true;
            this.lblAlumnoNuevoDNI.Location = new System.Drawing.Point(27, 141);
            this.lblAlumnoNuevoDNI.Name = "lblAlumnoNuevoDNI";
            this.lblAlumnoNuevoDNI.Size = new System.Drawing.Size(26, 13);
            this.lblAlumnoNuevoDNI.TabIndex = 9;
            this.lblAlumnoNuevoDNI.Text = "DNI";
            // 
            // txtAlumnoNuevoDNI
            // 
            this.txtAlumnoNuevoDNI.Location = new System.Drawing.Point(128, 134);
            this.txtAlumnoNuevoDNI.Name = "txtAlumnoNuevoDNI";
            this.txtAlumnoNuevoDNI.Size = new System.Drawing.Size(209, 20);
            this.txtAlumnoNuevoDNI.TabIndex = 10;
            // 
            // lblAlumnoNuevoNombreError
            // 
            this.lblAlumnoNuevoNombreError.AutoSize = true;
            this.lblAlumnoNuevoNombreError.Location = new System.Drawing.Point(363, 29);
            this.lblAlumnoNuevoNombreError.Name = "lblAlumnoNuevoNombreError";
            this.lblAlumnoNuevoNombreError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoNombreError.TabIndex = 17;
            // 
            // lblAlumnoNuevoApellidoError
            // 
            this.lblAlumnoNuevoApellidoError.AutoSize = true;
            this.lblAlumnoNuevoApellidoError.Location = new System.Drawing.Point(363, 66);
            this.lblAlumnoNuevoApellidoError.Name = "lblAlumnoNuevoApellidoError";
            this.lblAlumnoNuevoApellidoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoApellidoError.TabIndex = 18;
            // 
            // lblAlumnoNuevoLegajoError
            // 
            this.lblAlumnoNuevoLegajoError.AutoSize = true;
            this.lblAlumnoNuevoLegajoError.Location = new System.Drawing.Point(363, 99);
            this.lblAlumnoNuevoLegajoError.Name = "lblAlumnoNuevoLegajoError";
            this.lblAlumnoNuevoLegajoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoLegajoError.TabIndex = 19;
            // 
            // lblAlumnoNuevoDniError
            // 
            this.lblAlumnoNuevoDniError.AutoSize = true;
            this.lblAlumnoNuevoDniError.Location = new System.Drawing.Point(363, 133);
            this.lblAlumnoNuevoDniError.Name = "lblAlumnoNuevoDniError";
            this.lblAlumnoNuevoDniError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoDniError.TabIndex = 20;
            // 
            // lblAlumnoNuevoCursoError
            // 
            this.lblAlumnoNuevoCursoError.AutoSize = true;
            this.lblAlumnoNuevoCursoError.Location = new System.Drawing.Point(479, 174);
            this.lblAlumnoNuevoCursoError.Name = "lblAlumnoNuevoCursoError";
            this.lblAlumnoNuevoCursoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoCursoError.TabIndex = 21;
            // 
            // lblAlumnoNuevoError
            // 
            this.lblAlumnoNuevoError.AutoSize = true;
            this.lblAlumnoNuevoError.Location = new System.Drawing.Point(108, 223);
            this.lblAlumnoNuevoError.Name = "lblAlumnoNuevoError";
            this.lblAlumnoNuevoError.Size = new System.Drawing.Size(0, 13);
            this.lblAlumnoNuevoError.TabIndex = 22;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 254);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 26;
            // 
            // Datos
            // 
            this.Datos.Controls.Add(this.textBox2);
            this.Datos.Controls.Add(this.btnAlumnoNuevoBuscarUsuario);
            this.Datos.Controls.Add(this.btnAlumnoNuevoBuscarCurso);
            this.Datos.Controls.Add(this.lblAlumnoNuevoCursoError);
            this.Datos.Controls.Add(this.textBox1);
            this.Datos.Controls.Add(this.lblAlumnoNuevoUsuario);
            this.Datos.Controls.Add(this.lblAlumnoNuevoCurso);
            this.Datos.Controls.Add(this.txtAlumnoNuevoNombre);
            this.Datos.Controls.Add(this.lblAlumnoNuevoNombre);
            this.Datos.Controls.Add(this.txtAlumnoNuevoApellido);
            this.Datos.Controls.Add(this.txtAlumnoNuevoLegajo);
            this.Datos.Controls.Add(this.lblAlumnoNuevoDniError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoLegajoError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoApellido);
            this.Datos.Controls.Add(this.lblAlumnoNuevoApellidoError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoLegajo);
            this.Datos.Controls.Add(this.lblAlumnoNuevoNombreError);
            this.Datos.Controls.Add(this.lblAlumnoNuevoDNI);
            this.Datos.Controls.Add(this.txtAlumnoNuevoDNI);
            this.Datos.Location = new System.Drawing.Point(12, 12);
            this.Datos.Name = "Datos";
            this.Datos.Size = new System.Drawing.Size(547, 239);
            this.Datos.TabIndex = 27;
            this.Datos.TabStop = false;
            this.Datos.Text = "Datos";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(128, 204);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(209, 20);
            this.textBox2.TabIndex = 29;
            // 
            // btnAlumnoNuevoBuscarUsuario
            // 
            this.btnAlumnoNuevoBuscarUsuario.Location = new System.Drawing.Point(386, 201);
            this.btnAlumnoNuevoBuscarUsuario.Name = "btnAlumnoNuevoBuscarUsuario";
            this.btnAlumnoNuevoBuscarUsuario.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoNuevoBuscarUsuario.TabIndex = 30;
            this.btnAlumnoNuevoBuscarUsuario.Text = "Buscar";
            this.btnAlumnoNuevoBuscarUsuario.UseVisualStyleBackColor = true;
            this.btnAlumnoNuevoBuscarUsuario.Click += new System.EventHandler(this.btnAlumnoNuevoBuscarUsuario_Click);
            // 
            // btnAlumnoNuevoBuscarCurso
            // 
            this.btnAlumnoNuevoBuscarCurso.Location = new System.Drawing.Point(386, 163);
            this.btnAlumnoNuevoBuscarCurso.Name = "btnAlumnoNuevoBuscarCurso";
            this.btnAlumnoNuevoBuscarCurso.Size = new System.Drawing.Size(75, 23);
            this.btnAlumnoNuevoBuscarCurso.TabIndex = 23;
            this.btnAlumnoNuevoBuscarCurso.Text = "Buscar";
            this.btnAlumnoNuevoBuscarCurso.UseVisualStyleBackColor = true;
            this.btnAlumnoNuevoBuscarCurso.Click += new System.EventHandler(this.btnAlumnoNuevoBuscarCurso_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 167);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(209, 20);
            this.textBox1.TabIndex = 22;
            // 
            // lblAlumnoNuevoUsuario
            // 
            this.lblAlumnoNuevoUsuario.AutoSize = true;
            this.lblAlumnoNuevoUsuario.Location = new System.Drawing.Point(27, 211);
            this.lblAlumnoNuevoUsuario.Name = "lblAlumnoNuevoUsuario";
            this.lblAlumnoNuevoUsuario.Size = new System.Drawing.Size(56, 13);
            this.lblAlumnoNuevoUsuario.TabIndex = 28;
            this.lblAlumnoNuevoUsuario.Text = "USUARIO";
            // 
            // lblAlumnoNuevoCurso
            // 
            this.lblAlumnoNuevoCurso.AutoSize = true;
            this.lblAlumnoNuevoCurso.Location = new System.Drawing.Point(27, 173);
            this.lblAlumnoNuevoCurso.Name = "lblAlumnoNuevoCurso";
            this.lblAlumnoNuevoCurso.Size = new System.Drawing.Size(45, 13);
            this.lblAlumnoNuevoCurso.TabIndex = 21;
            this.lblAlumnoNuevoCurso.Text = "CURSO";
            // 
            // frmAlumnoNuevo
            // 
            this.AcceptButton = this.btnAlumnoNuevo;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnAlumnoNuevoCancelar;
            this.ClientSize = new System.Drawing.Size(571, 317);
            this.Controls.Add(this.Datos);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblAlumnoNuevoError);
            this.Controls.Add(this.btnAlumnoNuevoCancelar);
            this.Controls.Add(this.btnAlumnoNuevoLimpiar);
            this.Controls.Add(this.btnAlumnoNuevo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAlumnoNuevo";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alumno Nuevo";
            this.Datos.ResumeLayout(false);
            this.Datos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAlumnoNuevoNombre;
        private System.Windows.Forms.Button btnAlumnoNuevo;
        private System.Windows.Forms.Button btnAlumnoNuevoLimpiar;
        private System.Windows.Forms.Button btnAlumnoNuevoCancelar;
        private System.Windows.Forms.TextBox txtAlumnoNuevoNombre;
        private System.Windows.Forms.TextBox txtAlumnoNuevoApellido;
        private System.Windows.Forms.TextBox txtAlumnoNuevoLegajo;
        private System.Windows.Forms.Label lblAlumnoNuevoApellido;
        private System.Windows.Forms.Label lblAlumnoNuevoLegajo;
        private System.Windows.Forms.Label lblAlumnoNuevoDNI;
        private System.Windows.Forms.TextBox txtAlumnoNuevoDNI;
        private System.Windows.Forms.Label lblAlumnoNuevoNombreError;
        private System.Windows.Forms.Label lblAlumnoNuevoApellidoError;
        private System.Windows.Forms.Label lblAlumnoNuevoLegajoError;
        private System.Windows.Forms.Label lblAlumnoNuevoDniError;
        private System.Windows.Forms.Label lblAlumnoNuevoCursoError;
        private System.Windows.Forms.Label lblAlumnoNuevoError;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.GroupBox Datos;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnAlumnoNuevoBuscarUsuario;
        private System.Windows.Forms.Button btnAlumnoNuevoBuscarCurso;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblAlumnoNuevoUsuario;
        private System.Windows.Forms.Label lblAlumnoNuevoCurso;
    }
}