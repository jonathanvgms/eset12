﻿namespace EstadisticasEscuelaFrontEnd.Cursos
{
    partial class frmCursoModificar
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCursoModificar));
            this.btnCursoModificarLimpiar = new System.Windows.Forms.Button();
            this.btnCursoModificarAceptar = new System.Windows.Forms.Button();
            this.btnCursoModificarCancelar = new System.Windows.Forms.Button();
            this.lblCursoNuevoAnio = new System.Windows.Forms.Label();
            this.lblCursoNuevoDivision = new System.Windows.Forms.Label();
            this.txtCursoModificarAnio = new System.Windows.Forms.TextBox();
            this.txtCursoModificarDivision = new System.Windows.Forms.TextBox();
            this.lblCursoModificarTurno = new System.Windows.Forms.Label();
            this.lblCursoModificarEspecialidad = new System.Windows.Forms.Label();
            this.cmbCursoModificarTurno = new System.Windows.Forms.ComboBox();
            this.cmbCursoModificarEspecialidad = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnCursoModificarLimpiar
            // 
            this.btnCursoModificarLimpiar.Location = new System.Drawing.Point(13, 216);
            this.btnCursoModificarLimpiar.Name = "btnCursoModificarLimpiar";
            this.btnCursoModificarLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnCursoModificarLimpiar.TabIndex = 0;
            this.btnCursoModificarLimpiar.Text = "Limpiar";
            this.btnCursoModificarLimpiar.UseVisualStyleBackColor = true;
            this.btnCursoModificarLimpiar.Click += new System.EventHandler(this.btnCursoModificarLimpiar_Click);
            // 
            // btnCursoModificarAceptar
            // 
            this.btnCursoModificarAceptar.Location = new System.Drawing.Point(252, 216);
            this.btnCursoModificarAceptar.Name = "btnCursoModificarAceptar";
            this.btnCursoModificarAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnCursoModificarAceptar.TabIndex = 1;
            this.btnCursoModificarAceptar.Text = "Aceptar";
            this.btnCursoModificarAceptar.UseVisualStyleBackColor = true;
            this.btnCursoModificarAceptar.Click += new System.EventHandler(this.btnCursoModificarAceptar_Click);
            // 
            // btnCursoModificarCancelar
            // 
            this.btnCursoModificarCancelar.Location = new System.Drawing.Point(359, 216);
            this.btnCursoModificarCancelar.Name = "btnCursoModificarCancelar";
            this.btnCursoModificarCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCursoModificarCancelar.TabIndex = 2;
            this.btnCursoModificarCancelar.Text = "Cancelar";
            this.btnCursoModificarCancelar.UseVisualStyleBackColor = true;
            this.btnCursoModificarCancelar.Click += new System.EventHandler(this.btnCursoModificarCancelar_Click);
            // 
            // lblCursoNuevoAnio
            // 
            this.lblCursoNuevoAnio.AutoSize = true;
            this.lblCursoNuevoAnio.Location = new System.Drawing.Point(24, 30);
            this.lblCursoNuevoAnio.Name = "lblCursoNuevoAnio";
            this.lblCursoNuevoAnio.Size = new System.Drawing.Size(26, 13);
            this.lblCursoNuevoAnio.TabIndex = 3;
            this.lblCursoNuevoAnio.Text = "Año";
            // 
            // lblCursoNuevoDivision
            // 
            this.lblCursoNuevoDivision.AutoSize = true;
            this.lblCursoNuevoDivision.Location = new System.Drawing.Point(24, 65);
            this.lblCursoNuevoDivision.Name = "lblCursoNuevoDivision";
            this.lblCursoNuevoDivision.Size = new System.Drawing.Size(44, 13);
            this.lblCursoNuevoDivision.TabIndex = 4;
            this.lblCursoNuevoDivision.Text = "Division";
            // 
            // txtCursoModificarAnio
            // 
            this.txtCursoModificarAnio.Location = new System.Drawing.Point(209, 23);
            this.txtCursoModificarAnio.Name = "txtCursoModificarAnio";
            this.txtCursoModificarAnio.Size = new System.Drawing.Size(100, 20);
            this.txtCursoModificarAnio.TabIndex = 5;
            // 
            // txtCursoModificarDivision
            // 
            this.txtCursoModificarDivision.Location = new System.Drawing.Point(209, 65);
            this.txtCursoModificarDivision.Name = "txtCursoModificarDivision";
            this.txtCursoModificarDivision.Size = new System.Drawing.Size(100, 20);
            this.txtCursoModificarDivision.TabIndex = 6;
            // 
            // lblCursoModificarTurno
            // 
            this.lblCursoModificarTurno.AutoSize = true;
            this.lblCursoModificarTurno.Location = new System.Drawing.Point(27, 104);
            this.lblCursoModificarTurno.Name = "lblCursoModificarTurno";
            this.lblCursoModificarTurno.Size = new System.Drawing.Size(35, 13);
            this.lblCursoModificarTurno.TabIndex = 7;
            this.lblCursoModificarTurno.Text = "Turno";
            // 
            // lblCursoModificarEspecialidad
            // 
            this.lblCursoModificarEspecialidad.AutoSize = true;
            this.lblCursoModificarEspecialidad.Location = new System.Drawing.Point(27, 139);
            this.lblCursoModificarEspecialidad.Name = "lblCursoModificarEspecialidad";
            this.lblCursoModificarEspecialidad.Size = new System.Drawing.Size(67, 13);
            this.lblCursoModificarEspecialidad.TabIndex = 8;
            this.lblCursoModificarEspecialidad.Text = "Especialidad";
            // 
            // cmbCursoModificarTurno
            // 
            this.cmbCursoModificarTurno.FormattingEnabled = true;
            this.cmbCursoModificarTurno.Location = new System.Drawing.Point(209, 95);
            this.cmbCursoModificarTurno.Name = "cmbCursoModificarTurno";
            this.cmbCursoModificarTurno.Size = new System.Drawing.Size(121, 21);
            this.cmbCursoModificarTurno.TabIndex = 9;
            // 
            // cmbCursoModificarEspecialidad
            // 
            this.cmbCursoModificarEspecialidad.FormattingEnabled = true;
            this.cmbCursoModificarEspecialidad.Location = new System.Drawing.Point(209, 139);
            this.cmbCursoModificarEspecialidad.Name = "cmbCursoModificarEspecialidad";
            this.cmbCursoModificarEspecialidad.Size = new System.Drawing.Size(121, 21);
            this.cmbCursoModificarEspecialidad.TabIndex = 10;
            // 
            // frmCursoModificar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 251);
            this.Controls.Add(this.cmbCursoModificarEspecialidad);
            this.Controls.Add(this.cmbCursoModificarTurno);
            this.Controls.Add(this.lblCursoModificarEspecialidad);
            this.Controls.Add(this.lblCursoModificarTurno);
            this.Controls.Add(this.txtCursoModificarDivision);
            this.Controls.Add(this.txtCursoModificarAnio);
            this.Controls.Add(this.lblCursoNuevoDivision);
            this.Controls.Add(this.lblCursoNuevoAnio);
            this.Controls.Add(this.btnCursoModificarCancelar);
            this.Controls.Add(this.btnCursoModificarAceptar);
            this.Controls.Add(this.btnCursoModificarLimpiar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCursoModificar";
            this.Text = "CursoModificar";
            this.Load += new System.EventHandler(this.frmCursoModificar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCursoModificarLimpiar;
        private System.Windows.Forms.Button btnCursoModificarAceptar;
        private System.Windows.Forms.Button btnCursoModificarCancelar;
        private System.Windows.Forms.Label lblCursoNuevoAnio;
        private System.Windows.Forms.Label lblCursoNuevoDivision;
        private System.Windows.Forms.TextBox txtCursoModificarAnio;
        private System.Windows.Forms.TextBox txtCursoModificarDivision;
        private System.Windows.Forms.Label lblCursoModificarTurno;
        private System.Windows.Forms.Label lblCursoModificarEspecialidad;
        private System.Windows.Forms.ComboBox cmbCursoModificarTurno;
        private System.Windows.Forms.ComboBox cmbCursoModificarEspecialidad;
    }
}